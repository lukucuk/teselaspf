
-- SIMBOLOGIA
-- 1 = blanco 
-- 2 = gris 
-- 3 = celeste
-- 4 = turquesa

-- Descripcion de Teselas
-- a = parte superior
-- b = parte derecha
-- c = parte inferior
-- d = parte izquierda

-- PARA CREAR LISTA
--let tese = [(1,2,2,1),(2,1,1,2),(2,2,1,1),(1,1,2,2),(2,2,3,3),(1,1,2,2),(1,2,2,1),(2,3,3,2),(3,3,4,4),(2,2,3,3),(2,3,3,2),(3,4,4,3),(4,4,1,1),(3,3,4,4),(3,4,4,3),(4,1,1,4),(1,1,2,2),(4,4,1,1),(4,1,1,4),(1,2,2,1)]

--restricciones sobre la matriz de origen
--teselas blanco y gris
--para teselas que ((a y b = 1 blanco) y (c y d = 2 gris)) ROTAR ANTIHORARIO 2 VECES (ejemplo tesela posicion 4, 6 o 17)
--para teselas que ((d y a = 1 blanco) y (b y c= gris)) ROTAR ANTIHORARIO 1 VEZ (ejemplo tesela 1, 7 o 20) 
--para teselas que ((a y b = 2 gris) y (c y d = 1 blanco)) NO ROTAR (ejemplo tesela 3) (2,2,1,1)
--para teselas que ((b y c = 1 blanco y (d y a = 2 gris)) GIRO HORARIO 1 VEZ (ejemplo tesela 2) (2,1,1,2)

--teselas gris y celeste 
--para teselas que ((a y b = 2 gris) y (c y d = 3 celeste)) ROTAR ANTIHORARIO 2 VECES (ejemplo tesela 5)
--para teselas que ((b y c = 3 celeste) y ((d y a = 2 gris)) ROTAR ANTIHORARIO 1 VEZ (ejemplo tesela 8)

--teselas celeste y turquesas
--para teselas que ((a y b = 3 celeste) y (c y d = 4 turquesa)) ROTAR ANTIHORARIO 2 VECES (ejemplo tesela 9)
--para teselas que ((b y c = 3 celeste) y (d y a = 4 turquesa)) ROTAR ANTIHORARIO 1 VEZ (ejemplo tesela 12)

--teselas blanco y turquesas 
-- para teselas que ((a y b = 4 turquesa) y (c y d= 1 blanco)) ROTAR ANTIHORARIO 2 VECES (ejemplo tesela 13 o 18)
-- para teselas que ((b y c = 1 blanco ) y (d y a = 4 turquesa)) ROTAR ANTIHORARIO 1 VEZ (ejemplo tesela 16 o 19)


-- funcion de rotacion 1 vez horario
rotarUnaCuadrupla :: (Integer,Integer,Integer,Integer) -> (Integer,Integer,Integer,Integer)
rotarUnaCuadrupla (a,b,c,d)= (a,d,c,b)

-- Verifico Teselas especiales
teselaEspecial :: (Integer,Integer,Integer,Integer) -> (Integer,Integer,Integer,Integer)
teselaEspecial (a,b,c,d)= if ((a,b,c,d) == (1,2,2,1)) then -- si la 1,7 o 20
                          rotarUnaAntiCuadrupla (1,2,2,1)
                          else 
                            if  (a,b,c,d) == (2,1,1,2) then -- si el la tesela 2
                                rotarUnaCuadrupla (2,1,1,2)
                            else
                                (2,2,1,1)
                        

--(2,1,1,2)
-- funcion de rotacion 1 vez antihorario
rotarUnaAntiCuadrupla :: (Integer,Integer,Integer,Integer) -> (Integer,Integer,Integer,Integer)
rotarUnaAntiCuadrupla (a,b,c,d)= (b,c,a,d)

-- funcion de rotacion 2 veces antihorario
rotarDosAntiCuadrupla :: (Integer,Integer,Integer,Integer) -> (Integer,Integer,Integer,Integer)
rotarDosAntiCuadrupla (a,b,c,d)= (c,d,a,b)

--prueba de extraccion blancas y grises 
teseBlancaGris :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
teseBlancaGris tese = [ if x==(1,2,2,1)||x==(2,2,1,1)||x==(1,1,2,2)||x==(2,1,1,2) 
    then (1,2,2,1) else (0,0,0,0)  |x<-tese ]

--extraigo teselas blancas y grises
teseBlancaGris2 :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
teseBlancaGris2 tese = [ x |x<-tese, x `elem` [(1,2,2,1),(2,2,1,1),(2,1,1,2),(1,1,2,2)] ]

--extraigo teselas celestes y grises
teseCelesteGris :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
teseCelesteGris tese = [ x |x<-tese, x `elem` [(3,2,2,3),(2,2,3,3),(2,3,3,2),(3,3,2,2)] ]

--extraigo teselas celestes y turquesas
teseCelesteTurquesa :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
teseCelesteTurquesa tese = [ x |x<-tese, x `elem` [(3,3,4,4),(4,4,3,3),(4,3,3,4),(3,4,4,3)] ]

--extraigo teselas blancas y turquesas
teseBlancaTurquesa :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
teseBlancaTurquesa tese = [ x |x<-tese, x `elem` [(1,4,4,1),(4,4,1,1),(4,1,1,4),(1,1,4,4)] ]

---Graficar Listas Enteras BLANCAS Y GRISES
blancaGrises :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
listBG = teseBlancaGris2 tese
blancaGrises ordBG = [ if i==(1,1,2,2) ---por ejemplo 4,6,17
                         then rotarDosAntiCuadrupla i    
                         else teselaEspecial i  --ejemplo 1,7,20,2
                        |i<-listBG ]

---Graficar Listas Enteras CELESTES Y GRISES
celesteGrises :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
listCG = teseCelesteGris tese
celesteGrises ordCG = [ if i==(2,2,3,3)
                         then  rotarDosAntiCuadrupla i --ejemplo tesela 5 
                         else rotarUnaAntiCuadrupla i  -- ejemplo tesela 8
                        |i<-listCG ]

---Graficar Listas Enteras CELESTES Y TURQUESAS
celestesTurquesas:: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
listCT = teseCelesteTurquesa tese
celestesTurquesas ordCT = [ if i==(3,3,4,4)
                         then  rotarDosAntiCuadrupla i  --ejemplo tesela 9  
                         else rotarUnaAntiCuadrupla i  -- ejemplo tesela 12
                        |i<-listCT ]

---Graficar Listas BLANCAS Y TURQUESAS -- OK
blancaTurquesa :: [(Integer,Integer,Integer,Integer)] -> [(Integer,Integer,Integer,Integer)] 
listBT = teseBlancaTurquesa tese
blancaTurquesa ordBT = [ if i==(4,4,1,1)
                         then  rotarDosAntiCuadrupla i    
                         else rotarUnaAntiCuadrupla i  
                        |i<-listBT ]

--- Separacion e insercion de listas blancas y grises
extraerInsertar1 :: Integer -> [(Integer,Integer,Integer,Integer)]
a= blancaGrises tese
re = [(a!!0)] ++ [(a!!1)] ++ [(a!!2)] ++ [(a!!3)]
extraerInsertar1 b = re

extraerInsertar2 :: Integer -> [(Integer,Integer,Integer,Integer)]
x= blancaGrises tese
ret = [(x!!4)] ++ [(x!!5)] ++ [(x!!6)] ++ [(x!!7)]
extraerInsertar2 b = ret

--Control de simetria
--pattern matching
primer ::(Integer,Integer,Integer,Integer) -> Integer
primer (a,b,c,d) = a
segundo ::(Integer,Integer,Integer,Integer) -> Integer
segundo (a,b,c,d) = b
tercer :: (Integer,Integer,Integer,Integer) -> Integer
tercer (a,b,c,d) = c
cuarto :: (Integer,Integer,Integer,Integer) -> Integer
cuarto (a,b,c,d) = d


simetria :: String -> String
e = start "  "
simetria sim = if( tercer (e!!0) == (primer (e!!4))   && ((tercer (e!!1)) ==  (primer (e!!5)))  &&
                  ((tercer (e!!2)) == (primer (e!!6)))  && ((tercer (e!!3)) ==  (primer (e!!7)))  &&
                  ((tercer (e!!4)) == (primer (e!!8)))  && ((tercer (e!!5)) ==  (primer (e!!9)))  &&
                  ((tercer (e!!6)) == (primer (e!!10))) && ((tercer (e!!7)) ==  (primer (e!!11))) &&
                  ((tercer (e!!8)) == (primer (e!!12))) && ((tercer (e!!9)) ==  (primer (e!!13))) &&
                  ((tercer (e!!10)) == (primer (e!!14))) && ((tercer (e!!11)) == (primer (e!!15)))&&
                  ((tercer (e!!12)) == (primer (e!!16))) && ((tercer (e!!13)) == (primer (e!!17)))&&
                  ((tercer (e!!14)) == (primer (e!!18))) && ((tercer (e!!15)) == (primer (e!!19)))
                ) 
               then  "HAY SIMETRIA"
               else  "No hay simetria"

---damos comienzo al progama ingresando la lista predefinida - devuelve lista graficada en escenario final
start :: String -> [(Integer,Integer,Integer,Integer)]
tese = [(1,2,2,1),(2,1,1,2),(2,2,1,1),(1,1,2,2),
        (2,2,3,3),(1,1,2,2),(1,2,2,1),(2,3,3,2),
        (3,3,4,4),(2,2,3,3),(2,3,3,2),(3,4,4,3),
        (4,4,1,1),(3,3,4,4),(3,4,4,3),(4,1,1,4),
        (1,1,2,2),(4,4,1,1),(4,1,1,4),(1,2,2,1)]
m1 = extraerInsertar1 0
m2 = extraerInsertar2 0
b=blancaTurquesa tese
c= celestesTurquesas tese
d = celesteGrises tese
start x = m1 ++ b  ++ c ++ d ++ m2
